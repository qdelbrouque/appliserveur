package dto.commande;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DtoCommande {
    
    private Long    numcom;
    private Long    numcli;
    private String  nomcli;
    private String  adrcli;
    private String  codereg;
    private String  nomreg;
    private Float   tauxrem;
    private String  datecom;
    private String  etatcom;
    private Float   montantcommandeht;
    private Float   montanttva;
    private Float   montantCommandeTTC;
    private boolean estreglee;
    private Float   totalNet;

    
    public Long getNumcli() {
        return numcli;
    }

    public void setNumcli(Long numcli) {
        this.numcli = numcli;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getAdrcli() {
        return adrcli;
    }

    public void setAdrcli(String adrcli) {
        this.adrcli = adrcli;
    }

    public String getCodereg() {
        return codereg;
    }

    public void setCodereg(String codereg) {
        this.codereg = codereg;
    }

    public String getNomreg() {
        return nomreg;
    }

    public void setNomreg(String nomreg) {
        this.nomreg = nomreg;
    }

    public Float getTauxrem() {
        return tauxrem;
    }

    public void setTauxrem(Float tauxrem) {
        this.tauxrem = tauxrem;
    }

    public Float getTotalNet() {
        return totalNet;
    }

    public void setTotalNet(Float totalNet) {
        this.totalNet = totalNet;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    
    public Long getNumcom() {
        return numcom;
    }

    public void setNumcom(Long numcom) {
        this.numcom = numcom;
    }

    public String getDatecom() {
        return datecom;
    }

    public void setDatecom(String datecom) {
        this.datecom = datecom;
    }

    public String getEtatcom() {
        return etatcom;
    }

    public void setEtatcom(String etatcom) {
        this.etatcom = etatcom;
    }

    public Float getMontantcommandeht() {
        return montantcommandeht;
    }

    public void setMontantcommandeht(Float montantcommandeht) {
        this.montantcommandeht = montantcommandeht;
    }

    public Float getMontanttva() {
        return montanttva;
    }

    public void setMontanttva(Float montanttva) {
        this.montanttva = montanttva;
    }

    public Float getMontantCommandeTTC() {
        return montantCommandeTTC;
    }

    public void setMontantCommandeTTC(Float montantCommandeTTC) {
        this.montantCommandeTTC = montantCommandeTTC;
    }

    public boolean isEstreglee() {
        return estreglee;
    }

    public void setEstreglee(boolean estreglee) {
        this.estreglee = estreglee;
    }
    
    //</editor-fold>   
    
}
