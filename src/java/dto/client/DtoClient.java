package dto.client;

import entites.Commande;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DtoClient {
    
    private Long   numcli;
    private String nomcli;
    private String adrcli;
    private List<Commande> lesCom;

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    
    public Long getNumcli() {
        return numcli;
    }

    public List<Commande> getLesCom() {
        return lesCom;
    }

    public void setLesCom(List<Commande> lesCom) {
        this.lesCom = lesCom;
    }

    public void setNumcli(Long numcli) {
        this.numcli = numcli;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getAdrcli() {
        return adrcli;
    }

    public void setAdrcli(String adrcli) {
        this.adrcli = adrcli;
    }
    
    //</editor-fold>  
    
}
