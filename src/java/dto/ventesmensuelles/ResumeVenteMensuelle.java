
package dto.ventesmensuelles;

public class ResumeVenteMensuelle {
    
    
    int    numMois;
    String nomMois;
    Float  ca;

    public int getNumMois() {
        return numMois;
    }

    public void setNumMois(int numMois) {
        this.numMois = numMois;
    }

    public String getNomMois() {
        return nomMois;
    }

    public void setNomMois(String nomMois) {
        this.nomMois = nomMois;
    }

    public Float getCa() {
        return ca;
    }

    public void setCa(Float ca) {
        this.ca = ca;
    }
    
    
    
    
    
}
