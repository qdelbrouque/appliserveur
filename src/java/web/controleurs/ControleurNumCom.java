package web.controleurs;

import dao.consultation.commande.DaoCommande;
import entites.Commande;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class ControleurNumCom {
    
    @Inject private DaoCommande daoCommande;
    
    private Long numCom;  
    private Commande commande = new Commande();
    private Float tvaClient;
    
    public void ecouteurRecherche(){
        setCommande(daoCommande.getCommande(getNumCom()));
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS & SETTERS">
        public Long getNumCom() {
        return numCom;
    }

    public void setNumCom(Long numCom) {
        this.numCom = numCom;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }
    //</editor-fold>
    
}
