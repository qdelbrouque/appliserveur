
package web.controleurs;


import dao.consultation.region.DaoRegion;
import entites.Region;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author rsmon
 */

@Named
@RequestScoped
public class ControleurStatsCaParRegions {
   
  @Inject
  private DaoRegion daoRegion;
 
    
  private PieChartModel camembert;
  
  @PostConstruct
  public void init(){
      
    camembert=new PieChartModel();
    camembert.setTitle("Chiifre d'affaires par Régions");
    camembert.setLegendPosition("w");
    camembert.setShowDataLabels(true);
    for (Region reg: daoRegion.getToutesLesRegions()){
       
        camembert.set(reg.getNomRegion(), reg.caAnneeEnCours());    
    }
  }

  public PieChartModel getCamembert() {
        return camembert;
    }
  
  //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
  
  //</editor-fold>
}
