
package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Users implements Serializable{
    
    @Id
    private String login;
    private String mdp;
    
    public Users() {};
    
    public Users(String pLogin, String pMdp){
        
        this.login = pLogin;
        this.mdp = pMdp;
        
    }

    

    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
        //</editor-fold>
}
