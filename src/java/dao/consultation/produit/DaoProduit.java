package dao.consultation.produit;

import entites.Produit;

/**
 *
 * @author rsmon
 */

public interface DaoProduit {

    Produit getProduit(String pRefProd);
    
}
