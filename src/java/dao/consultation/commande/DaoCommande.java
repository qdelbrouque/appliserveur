package dao.consultation.commande;

import entites.Commande;
import java.util.List;

public interface DaoCommande {

    Commande       getCommande(Long pNumCom);
    List<Commande> getToutesLesCommandes();    

    List<Commande> getLesCommandes( int pAnnee);
    List<Commande> getLesCommandes( int pAnnee, int pMois);

    List<Commande> getLesCommandes( int pAnnee, String pEtat);
    List<Commande> getLesCommandes( int pAnnee, int pMois, String pEtat);
   
}
