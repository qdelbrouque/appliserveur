
package dao.consultation.region;

import entites.Region;
import java.util.List;

public interface DaoRegion {

    Region             getLaRegion(String codeReg);
    List<Region>  getToutesLesRegions();
}
