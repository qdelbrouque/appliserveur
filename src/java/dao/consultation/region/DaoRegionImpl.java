package dao.consultation.region;

import entites.Region;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rsmon
 */

@Stateless
public class DaoRegionImpl  implements DaoRegion,Serializable {

    @PersistenceContext private EntityManager em;
    
    @Override
    public List<Region> getToutesLesRegions() {
     
        return em.createQuery("Select r from Region r").getResultList();
    }  
    
    @Override
    public Region getLaRegion(String codeReg) {
        return em.find(Region.class, codeReg);
    }
}
