package dao.consultation.catprod;

import entites.CategorieProduit;
import java.util.List;

public interface DaoCategorieProduit {
    List<CategorieProduit> getToutesLesCategoriesProduits();
}
