
package dao.consultation.users;
import entites.Users;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class DaoUsersImpl implements DaoUsers,Serializable {
    
    
    @PersistenceContext private EntityManager em;
    
    @Override
    public Users getUser(String login) {
        return em.find(Users.class, login);
    }
    
    
}
